<?php include ('db.php'); ?>

<?if($_POST['action'] == 'getmenu') {
    $g_id = $_POST['id'];
    echo "<div style='margin-left: 5px; color: #524e4e'>
          <div id='add_price_form'>
                <div class='menu_redac'><span>+Период : От</span><input id='add_start_date' class='date' type='text' value='' readonly></div>
                <div class='menu_redac'><span>До</span><input id='add_end_date' class='date' type='text' value='' readonly></div>
                <div class='menu_redac'><span>Цена</span><input id='add_price' type='text' value=''>
            <i class='fa fa-plus' style='margin-left: 5px' onclick='addPrice($g_id)'></i>
          </div></div>";
    
    $prices = getAllPrices($_POST['id']); $counter = 0; if(!$prices) echo "Периоды не заданы";

    foreach($prices as $price) { 
        $counter++;
        $id = $price['id'];
        $start_date = $price['start_date'];
        $end_date = $price['end_date'];
        $p_price = round($price['price'], 3);

        echo "<div class='prices' id='$id'>
                <div class='menu_redac'><span>Период: $counter От</span><input class='startdate' type='text' value='$start_date'></div>
                <div class='menu_redac'><span>До</span><input class='enddate' type='text' value='$end_date'></div>
                <div class='menu_redac'><span>Цена</span><input class='price' type='text' value='$p_price'>
                    <i class='fa fa-times' onclick='deletePrice($id)'></i></div>
                    <i class='fa fa-save' onclick='updatePrice($id)'></i>
             </div>";
    }
    $sort = getSort($g_id);

    if($sort['sort'] == 'date') 
    $checked1 = 'checked';
    else $checked2 = 'checked';

    echo "<div style='margin: 5px' align='center'><span>Приоритет определения цены: </span>
          <p><input id='$g_id' class='r' onclick='setSortD($g_id)' name='prior' type='radio' value='date' $checked1>Наименьший интервал</p>
          <p><input id='$g_id' class='r' onclick='setSortP($g_id)' name='prior' type='radio' value='pos' $checked2>Последняя добавленная</p></div>";

} elseif($_POST['action'] == 'delete') {
    deletePrice($_POST['id']);
} elseif($_POST['action'] == 'add') {
    addPrice($_POST['id'], $_POST['start_date'], $_POST['end_date'], $_POST['price']);
} elseif($_POST['action'] == 'update') {
    updatePrice($_POST['id'], $_POST['start_date'], $_POST['end_date'], $_POST['price']);
} elseif($_POST['action'] == 'setsort') {
    setSort($_POST['id'], $_POST['sort']);
} elseif($_POST['action'] == 'getsorted') {
         $products = getProductsWithSortedPrice($_POST['date']);
         foreach($products as $product) {
             $pic = $product['pic'];
             $name = $product['name'];
             $price = round($product['price'], 3)."руб.";
         echo "<div class='shop_item'>
               <img style='margin-left: 30%; margin-top: 10px;' src='./img/$pic' width='200' height='200'/>
               <div class='tex'>
                    <span>Название товара: $name</span><br>
                    <span>Цена: $price</span>
                </div>
            </div>";
         }
}
?>