
function openMenu(id){
    $('.m_edit').html('');
    $('#'+ id +'.m_edit').html('<img src="/img/loading.gif" width="100" height="100">');
    $.ajax({
        url: "edit.php",
        type: "POST",
        data: { id: id, action: 'getmenu' },
        success: function(data){
          $('#'+ id +'.m_edit').html(data);
          d_format = { dateFormat: 'yy-mm-dd' }
          $( ".startdate" ).datepicker(d_format);
          $( ".enddate" ).datepicker(d_format);
          $( ".date" ).datepicker(d_format);
        }
      });
}

function deletePrice(id) {
  $.ajax({
    url: "edit.php",
    type: "POST",
    data: { id: id, action: 'delete'},
    success: function(){
      $('#'+ id + '.prices').remove('#'+id);
    }
  });

}

function setSortD(id) {
  $.ajax({
    url: "edit.php",
    type: "POST",
    data: { id: id, sort: 'date', action: 'setsort'},
    success: function(){
    }
  });
}

function setSortP(id) {
  $.ajax({
    url: "edit.php",
    type: "POST",
    data: { id: id, sort: 'pos', action: 'setsort'},
    success: function(){
    }
  });
}

function updatePrice(id) {

  start = $('#' + id + '.prices').find('.startdate').val();
  end =  $('#' + id + '.prices').find('.enddate').val();
  price = $('#' + id + '.prices').find('.price').val();

  $.ajax({
    url: "edit.php",
    type: "POST",
    data: { id: id, start_date: start, end_date: end, price: price, action: 'update'},
    success: function(){
    }
  });

}

function getShopItems(date) {
  $.ajax({
    url: "edit.php",
    type: "POST",
    data: { date: date, action: 'getsorted' },
    success: function(data){
      $('#items').html(data);
    }
  });
}

function addPrice(id) {
  let start = '';
  let end = '';
  let price = '';

  start = $('#add_start_date').val();
  end = $('#add_end_date').val();
  price = $('#add_price').val();

  if(start != '' && end != '' && price != '') {
    $('#'+ id +'.m_edit').html('<img src="/imgs/loading.gif" width="100" height="100">');
    $.ajax({
      url: "edit.php",
      type: "POST",
      data: { id: id, start_date: start, end_date: end, price: price, action: 'add'},
      success: function(){
        openMenu(id);
      }
    });
  } else alert('Введите данные во все поля!');
}