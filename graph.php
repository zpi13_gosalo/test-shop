<?php include ('db.php'); ?>
<html>
<head>
    <title>Shop</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript" src="flot/jquery.flot.js"></script>
    <script language="javascript" type="text/javascript" src="flot/jquery.flot.time.js"></script>
    <script src="common.js"></script>
    <style>
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            overflow: auto;
    }
    </style>
</head>
<body>
<header>
    <span class="caption">Графики  изменения цен</span>
</header>
<div id="content">
    
</div>
<script>
var parsed = JSON.parse('<? echo getProductsWithSortedPriceGraph(); ?>');

for(var product = 0; product < parsed.length; product++) {
    var all_data = [
        { label: "Сортировка по наименьшему интервалу", color: 0, data: parsed[product].date_sort[0] },
        { label: "Сортировка по очередности", color: 1, data: parsed[product].pos_sort[0] }
    ];
    for(var i = 0; i < all_data.length; i++)
        for(var j = 0; j < all_data[i].data.length; ++j)
            all_data[i].data[j][0] = Date.parse(all_data[i].data[j][0]);

    var options = {
                    lines: { show: true },
                    points: { show: false },
                    xaxis: { mode: "time", timeformat: "%y-%m-%d" },
                    yaxis: { min:1, max: 25000},
                  };
    $('#content').append('<span>'+ parsed[product].name +'</span><div id="placeholder' + product + '" style="width:600px;height:300px;"></div>');
    $.plot($("#placeholder" + product), all_data, options);
}
    </script>
    
</body>
</html>







