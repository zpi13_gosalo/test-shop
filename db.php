<?php
    function dbConnect() { //соеденение с бд
        $servername = "mysql.zzz.com.ua";
        $username = "antong112";
        $password = "123456Qq";
        $dbname = "antong112";

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($connection->connect_error) die("Connection failed: " . $conn->connect_error);
        else return $conn;
    }

    function getAllProducts() { //получить весь список продуктов
        $connection = dbConnect();
        $sql = "SELECT * FROM products";
        $products = [];
        $data = $connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($products, $row);
        return $products;
    }

    function getAllPrices($id) { //получить все цены одного продукта
        $connection = dbConnect();
        $sql = "SELECT * FROM prices WHERE product_id=$id";
        $prices = [];
        $data = $connection->query($sql);
        while($row = $data->fetch_assoc()) array_push($prices, $row);
        return $prices;
    }

    function sortDate($id, $date, $connection) { //приоритет цены по наименьшему интервалу
        $sql = "SELECT price 
                FROM prices 
                WHERE product_id=$id  
                AND '$date'
                BETWEEN start_date 
                AND end_date 
                ORDER BY UNIX_TIMESTAMP(STR_TO_DATE(end_date, '%Y-%m-%d') - STR_TO_DATE(start_date, '%Y-%m-%d')) 
                ASC LIMIT 1";

        $price = $connection->query($sql)->fetch_assoc()['price'];
        return $price;
    }

    function sortPos($id, $date, $connection) { //приоритет цены по последней добавленной
        $sql = "SELECT price 
                FROM prices 
                WHERE product_id=$id 
                AND '$date' 
                BETWEEN start_date 
                AND end_date 
                ORDER BY id 
                DESC LIMIT 1";

        $price = $connection->query($sql)->fetch_assoc()['price'];
        return $price;
    }

    function getProductsWithSortedPrice($date) { //получить цены на все продукты с учетом сортировки
        $connection = dbConnect();
        $sql = "SELECT * FROM products";
        $data = $connection->query($sql);

        $products = [];
        $sorted = [];

        while($row = $data->fetch_assoc()) array_push($products, $row);

        foreach($products as $product) {
            $id = $product['id'];
            if($product['sort'] == 'date') {
                $price = sortDate($id, $date, $connection);

                if($price)$product['price'] = $price;
                array_push($sorted, $product);
            
            } else {
                $price = sortPos($id, $date, $connection);

                if($price)$product['price'] = $price;
                array_push($sorted, $product);
            }
        }

        return $sorted;
    }

    function getProductsWithSortedPriceGraph() { //получить графики изменения цен на продукты с учетом двух способов фильтрации
        $connection = dbConnect();
        $sql = "SELECT * FROM products";
        $data = $connection->query($sql);

        $products = [];
        while($row = $data->fetch_assoc()) array_push($products, $row);

        $sortedProducts = [];

        foreach($products as $product) {
            $product_id = $product['id'];

            $sql = "SELECT start_date, end_date 
            FROM prices 
            WHERE product_id=$product_id 
            ORDER BY UNIX_TIMESTAMP(STR_TO_DATE(end_date, '%Y-%m-%d') - STR_TO_DATE(start_date, '%Y-%m-%d')) 
            DESC LIMIT 1";

            $data = $connection->query($sql);

            $product_d = $data->fetch_assoc();
            $start = date_create($product_d['start_date']);
            $end = date_create($product_d['end_date']);

            $product_graph['date_sort'][0] = [];

            $product_graph['pos_sort'][0] = [];
            $product_graph['name'] = $product['name'];

            while($start != $end) {
            array_push($product_graph['date_sort'][0], array(0 => date_format($start, 'Y-m-d'), 
                        1 => sortDate($product_id, date_format($start, 'Y-m-d'), $connection)));

            array_push($product_graph['pos_sort'][0], array(0 => date_format($start, 'Y-m-d'), 
                        1 => sortPos($product_id, date_format($start, 'Y-m-d'), $connection)));

            date_modify($start, '+1 day');
            }
            array_push($sortedProducts, $product_graph);
        }

        return json_encode($sortedProducts);
    }

    function deletePrice($id) { //удалить цену у продукта
        $connection = dbConnect();
        $sql = "DELETE FROM prices WHERE id=$id";
        $connection->query($sql);
    }

    function setSort($id, $sort) { //установить режим сотрировки цены продукта
        $connection = dbConnect();
        $sql = "UPDATE products SET sort='$sort' WHERE id=$id";
        $connection->query($sql);
    }

    function getSort($id) { //получить текущий режим сотрировки продукта
        $connection = dbConnect();
        $sql = "SELECT sort FROM products WHERE id=$id";
        return $connection->query($sql)->fetch_assoc();
    }

    function updatePrice($id, $start_date, $end_date, $price) { //обновить цену
        $connection = dbConnect();
        $sql = "UPDATE prices SET start_date='$start_date', end_date='$end_date', price=$price
                WHERE id=$id";
        $connection->query($sql);
    }

    function addPrice($product_id, $start_date, $end_date, $price) { //добавить новую цену
        $connection = dbConnect();
        $sql = "INSERT INTO prices (product_id, start_date, end_date, price)
                VALUES ('$product_id', '$start_date', '$end_date', '$price')";
        $connection->query($sql);
    }