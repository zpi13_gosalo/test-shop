<? include ('db.php'); ?>
<html>
<head>
    <title>Shop</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="common.js"></script>
    <script>
        $(function(){
            $('#thisday').datepicker({ dateFormat: 'yy-mm-dd' });
            $('#thisday_btn').click(function(){
                getShopItems($('#thisday').val());
            });
            getShopItems('2018-11-11');
        });

    </script>
</head>
<body>
<header>
    <span class="caption">Изменение цен на товары в магазине</span>
</header>
<div id="content">
    <div id="left_side">
        <span class="caption">Изменение цены</span>
        <div class="items">
        <?$products = getAllProducts();
         foreach($products as $product) {?>
            <div class="shop_item" id="<?=$product["id"]?>">
                <img src="./img/<?=$product["pic"]?>" width="80" height="80"/>
                <div class="tex">
                    <span>Название товара: <?=$product["name"]?></span><br>
                    <span>Цена по умолчанию: <?=round($product["price"], 3)?>руб.</span>
                </div>
                <div class="edit">
                    <i class="fas fa-pen" onclick="openMenu('<?=$product["id"]?>')"></i>
                </div>
                <div class="m_edit" id="<?=$product["id"]?>">
                </div>
            </div>
         <?}?>
        </div>
    </div>
    <div id ="right_side">
        <span class="caption">Товар в магазине</span>
        <div style='margin-left: 30%; margin-top: 5px'>
            <input id='thisday_btn' type='button' value='Показать цены'>
            <input style='width: 100px' id='thisday' type='text' value='Выбрать день'>
            <input id='graph' type='button' onclick='document.location.href="/graph.php"' value='Вывести графики'>
        </div>
        <div id="items">
        </div>
    </div>
</div>
</body>
</html>

